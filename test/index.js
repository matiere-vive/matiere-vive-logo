import svgjs from 'svg.js/dist/svg.min.js';
import {Logo, calculateDistance} from '../src/index.js';
const MATIEREVIVE = require('./matiere-vive.svg');
import {defaultAnimationIn, defaultAnimationOut} from '../src/index.js';
var draw = svgjs.get('logo');

const onLineStart = (line) => {
  const dot = MVLogo.getCoord(line.coords[0]).dot;
  dot.show();
}
const onLineEnd = (line) => {
  const dot = MVLogo.getCoord(line.coords[1]).dot;
  dot.show();
}

const animationIn = (line, container) => {
  return new Promise(resolve => {
    defaultAnimationIn(MVLogo, line, container, resolve);
  });
}
const animationOut = (line, container) => {
  return new Promise(resolve => {
    defaultAnimationOut(MVLogo, line, container, resolve);
  });
}

var MVLogo = new Logo(119,draw, {
  sync: true,
  hasText: true,
  text: MATIEREVIVE,
  colorScheme: 'black',
  // functions
  animationIn: animationIn,
  animationOut: animationOut,
  transition: 100,
  // event
  onLineStart: onLineStart,
  onLineEnd: onLineEnd,
});

MVLogo.switchColor('white');
MVLogo.generateNewLetters();
MVLogo.print();
// setTimeout(() => {
//   MVLogo.startLoop();
  
// }, 5000);