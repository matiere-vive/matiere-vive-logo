const merge = require('webpack-merge');
const common = require('./webpack.config.common.babel.js');

const devConfig = {
  mode: 'development',
  devtool: 'inline-source-map',
  output: {
    filename: 'test-bundle.js'
  }
};

module.exports = merge(common, devConfig);