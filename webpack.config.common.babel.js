import compose from 'lodash/fp/compose';
const path = require('path');
// const libraryName = 'matiere-vive-logo';
import svgInline from './src/webpack.config.babel.js';

const buildConfig = compose(
  svgInline
);

const defaultOptions = {
  entry: path.resolve(__dirname, 'test/index.js'),
  output: {
    path: path.resolve(__dirname, 'test'),
  }
};

var webpackConfig = buildConfig(defaultOptions);

module.exports = webpackConfig;