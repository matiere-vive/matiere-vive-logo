const merge = require('webpack-merge');
const common = require('./webpack.config.common.babel.js');
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

const prodOptions = {
  mode: 'production',
  plugins: [
    new UglifyJsPlugin({
      sourceMap: false
    })
  ],
  output: {
    filename: 'test-bundle.min.js',
  }
};

module.exports = merge(common, prodOptions);