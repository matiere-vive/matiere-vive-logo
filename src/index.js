/** @module matiere-vive-logo */
const {getRandomM, getRandomV, M, V} = require('./letters.js');
const VCOLOR = '#00FF00';
const VCOLORW = '#CDCDCD';
const VCOLORB = '#919191';
const MCOLOR = '#FF00FF';
const MCOLORW = '#6F6F6F';
const MCOLORB = '#323232';
const ROWS = 3;
const COLS = 3;
const DURATION = 300;
const TRANSITION = 4000;

/**
 * @param {number} distance - distance to draw
 *
 */
const calculateDuration = (distance) => {
  return distance;
}
/**
 * @param {number} x1 - coord on axis X
 * @param {number} y1 - coord on axix Y
 * @param {number} x2 - coord on axix X
 * @param {number} y2 - coord on axix Y
 * @returns {number}
 */
function calculateDistance (x1, y1, x2, y2){
    return Math.sqrt( (x2-=x1)*x2 + (y2-=y1)*y2 );
}
module.exports.calculateDistance = calculateDistance;

/**
 * default function to draw line
 * @param {<Logo>} logo - instance of Logo
 * @param {<Line>} line - instance of line
 * @param {<svg.js#Container>} container - svgjs container to draw line
 */
function defaultAnimationIn(logo, line, container, resolve){
  const dot1 = logo.getCoord(line.coords[0]);
  const dot2 = logo.getCoord(line.coords[1]);
  const distance = calculateDistance(dot1.x, dot1.y, dot2.x, dot2.y);
  const duration = calculateDuration(distance);
  line.line = container.line(dot1.x, dot1.y, dot1.x, dot1.y).stroke({ width: line.pathWidth, color:line.color })
  line.line.animate(duration).plot(dot1.x, dot1.y, dot2.x, dot2.y).afterAll(() => resolve(line.line));
}
module.exports.defaultAnimationIn = defaultAnimationIn;

/**
 * default function to print line
 * @param {<Logo>} logo - instance of Logo
 * @param {<Line>} line - instance of line
 * @param {<svg.js#Container>} container - svgjs container to draw line
 */
function defaultAnimationInStatic(logo, line, container, resolve){
  const dot1 = logo.getCoord(line.coords[0]);
  const dot2 = logo.getCoord(line.coords[1]);
  const distance = calculateDistance(dot1.x, dot1.y, dot2.x, dot2.y);
  const duration = calculateDuration(distance);
  line.line = container.line(dot1.x, dot1.y, dot2.x, dot2.y).stroke({ width: line.pathWidth, color:line.color });
}
module.exports.defaultAnimationInStatic = defaultAnimationInStatic;

/**
 * default function to imdraw line
 * @param {<Logo>} logo - instance of Logo
 * @param {<Line>} line - instance of line
 * @param {<svg.js#Container>} container - svgjs container to draw line
 */
function defaultAnimationOut(logo, line, container, resolve){
  const dot1 = logo.getCoord(line.coords[0]);
  const dot2 = logo.getCoord(line.coords[1]);
  const distance = calculateDistance(dot1.x, dot1.y, dot2.x, dot2.y);
  const duration = calculateDuration(distance);
  line.line.animate(duration).plot(dot2.x, dot2.y, dot2.x, dot2.y).afterAll(() => resolve(line.line));
};
module.exports.defaultAnimationOut = defaultAnimationOut;

/**
 * svg line on the grid
 * @constructor
 * @param {array} coords - Coordinates of line like 
 * @param {string} color - in hexColor
 * @param {number} pathWidth - in px
 * @param {object|object[]} options
 * @param {function} [null] options.animationIn - to draw line
 * @param {function} [null] options.animationOut - to undraw line
 * @param {function} [null] options.onLineStart - event triggered on line start
 * @param {function} [null] options.onLineEnd - event triggered on line end
 */
class Line {
  constructor(coords, color, pathWidth, options) {
    this.options = {
      // functions
      animationIn: null,
      animationOut: null,
      onLineStart: null,
      onLineEnd: null,
    };
    Object.assign(this.options, options);
    this._coords = coords;
    this._color = color;
    this._line = null;
    this._pathWidth = pathWidth;

    return this;
  }
  set color (color)   { this._color = color; }
  get color ()        { return this._color;}
  set coords (coords) { this._coords = coords; }
  get coords ()       { return this._coords;}
  set line (line)   { this._line = line; }
  get line ()        { return this._line;}
  set pathWidth (pathWidth)   { this._pathWidth = pathWidth; }
  get pathWidth ()        { return this._pathWidth;}

  draw(container) {
    var {animationIn, onLineStart, onLineEnd} = this.options;
    if (onLineStart!=null){
      onLineStart(this);
    }
    if (animationIn!=null){
      return animationIn(this, container);
    }
  }
  undraw(container) {
    var {animationOut} = this.options;
    if (animationOut!=null){
      return animationOut(this, container);
    } else {
      this.remove();
    }
  }
  remove() {
    if (this.line){
      this.line.remove();
    }
  }
}

/**
 * abstract letter on the grid
 * @constructor
 * @param {array} coords - Coordinates of letter 
 * @param {string} color - in hexColor
 * @param {number} pathWidth - in px
 * @param {object} options
 * @param {boolean} options.sync - does line print/draw syncronously?
 * @param {function} options.animationIn - to draw line
 * @param {function} options.animationOut - to undraw line
 * @param {function} options.onLineStart - event triggered on line start
 * @param {function} options.onLineEnd - event triggered on line end
 * @param {function} options.afterAll - event triggered when letter was drawn
 */
class Letter {
  constructor(coords, color, pathWidth, container, options){
    this.options = {
      sync: true,
      // functions
      animationIn: null,
      animationOut: null,
      // event
      afterAll: null,
      onLineStart: null,
      onLineEnd: null,
    };
    this.PATHWIDTH = pathWidth;
    Object.assign(this.options, options);
    this._coords = coords;
    this._color = color;
    this._container = container;
    this.lines = new Set();

    this.createLetter();

  }

  set color (color)   { this._color = color; }
  get color ()        { return this._color;}
  set coords (coords) { this._coords = coords;}
  get coords ()       { return this._coords;}
  set container (container) { this._container = container;}
  get container ()       { return this._container;}

  /**
   * @param {number} pathWidth - new path width to set
   */
  setPathWidth(pathWidth) {
    this.PATHWIDTH = pathWidth;
  }

  createLetter(newCoords=null) {
    const { animationIn, animationOut, onLineStart, onLineEnd } = this.options;
    const options = { animationIn, animationOut, onLineStart, onLineEnd };

    if (newCoords!=null){
      this.coords = newCoords;
    }
    this.lines.clear();
    for(let coord of this.coords) {
      if (typeof coord[0] == 'object'){
        const subLine = new Set();
        coord.forEach((subCoord) => {
          subLine.add(new Line(subCoord, this.color, this.PATHWIDTH, options));
        });
        this.lines.add(subLine);
      } else {
        this.lines.add(new Line(coord, this.color, this.PATHWIDTH, options));
      }
    }
  }

  /**
   * @param {function} [null] onComplete - event trigger after drawing
   */
  async draw(onComplete=null) {
    for (let line of this.lines){
      if (!this.options.sync) {
        if (typeof line.size == 'number'){
          for (let subLine of line){
            subLine.draw(this.container);
            if (this.options.onLineEnd!=null){
              this.options.onLineEnd(subLine);
            }
          }
        } else {
          line.draw(this.container);
        }
      } else {
        if (typeof line.size == 'number'){
          for (let subLine of line){
            subLine.draw(this.container);
            if (this.options.onLineEnd!=null){
              this.options.onLineEnd(subLine);
            }
          }
        } else {
          await line.draw(this.container);
          if (this.options.onLineEnd!=null){
            this.options.onLineEnd(line);
          }
        }
      }
    }
    if (onComplete!=null){
      onComplete();
    }
    if (this.options.afterAll!=null) {
      this.options.afterAll();
    }
  }

  /**
   * @param {function} [null] onComplete - event trigger after drawing
   */
  async undraw(onComplete=null) {
    for (let line of this.lines){
      if (!this.options.sync) {
        if (typeof line.size == 'number'){
          for (let subLine of line){
            await subLine.undraw();
          }
        } else {
          line.undraw();
        }
      } else {
        if (typeof line.size == 'number'){
          for (let subLine of line){
            await subLine.undraw();
          }
        } else {
          await line.undraw();
        }
      }
    }
    if (this.options.sync) {
      this.remove();
    }
    if (onComplete!=null){
      onComplete();
    }
  }
  remove() {
    for (let line of this.lines){
      if (typeof line.size == 'number'){
        for (let subLine of line){
          subLine.remove();
        }
      } else {
        line.remove();
      }
    }
  }
}

/**
 * abstract letter on the grid
 * @constructor
 * @param {number} width - width of the image
 * @param {<SVG>} draw - element to draw in
 * @param {object} options
 * @param {boolean} options.sync - does line print/draw asyncronously?
 * @param {boolean} options.hasText - has logo text
 * @param {<svgElement>} options.text - text element
 * @param {function} options.animationIn - to draw line
 * @param {function} options.animationOut - to undraw line
 * @param {function} options.onLineStart - event triggered on line start
 * @param {function} options.onLineEnd - event triggered on line end
 * @param {function} options.afterAll - event triggered when letter was drawn
 */
class Logo {

  constructor(width, svgjs, options) {
    this.options = {
      sync: true,
      hasText: false,
      colorScheme: 'color',
      text: null,
      transition: TRANSITION,
      transitionBetween: TRANSITION/100,
      // functions
      animationIn: null,
      animationOut: null,
      // event
      afterAll: null,
      onLineStart: null,
      onLineEnd: null,
    };
    this.svgjsDraw = svgjs;
    this.setDimensions(width);
    
    Object.assign(this.options, options);

    this._step = 0;
    if (typeof V !== 'undefined') {
      this._end = V.length;
    }
    this.showText = this.showText.bind(this);

    this.logoContainer = this.svgjsDraw.group();
    this.vContainer = this.svgjsDraw.group();
    this.mContainer = this.svgjsDraw.group();
    this.dotsContainer = this.svgjsDraw.group();
    this.texteContainer = this.svgjsDraw.group();

    this.drawGrid();

    var vCoords = this.generateLine(getRandomV());
    this.v = new Letter(vCoords, this.options.colorScheme === 'color' ? VCOLOR : this.options.colorScheme === 'black' ? VCOLORB : VCOLORW, this.PATHWIDTH, this.vContainer, {
      sync: this.options.sync,
      animationIn: this.options.animationIn,
      animationOut: this.options.animationOut,
      onLineStart: this.options.onLineStart,
      onLineEnd: this.options.onLineEnd,
    });

    var mCoords = this.generateLine(getRandomM());
    this.m = new Letter(mCoords, this.options.colorScheme === 'color' ? MCOLOR : this.options.colorScheme === 'white' ? MCOLORB : MCOLORW, this.PATHWIDTH, this.mContainer, {
      sync: this.options.sync,
      animationIn: this.options.animationIn,
      animationOut: this.options.animationOut,
      onLineStart: this.options.onLineStart,
      onLineEnd: this.options.onLineEnd,
    });

    if (this.options.hasText){
      this.addText(this.options.text, false);
    }
    
    this.logoContainer.add(this.mContainer);
    this.logoContainer.add(this.vContainer);
    this.logoContainer.add(this.dotsContainer);
    this.logoContainer.add(this.texteContainer);
    
  }

  get step()     { return this._step; }
  set step(step) { 
    if (step >= this.end)
      step = 0;
    this._step = step; 
  }
  get end()      { return this._end; }
  set end(end)   { this._end = end; }

  /**
   * set the resulting dimensions
   * @param {number} width - image width
   */
  setDimensions(width){
    this.WIDTH = width;
    // this.WIDTH = ;
    this.GUTTER = this.WIDTH*20/120; 
    this.HEIGHT = this.WIDTH*3/4;
    if (this.WIDTH<=16){
      this.CIRCLEWIDTH = this.WIDTH/121*9;
    } else if (this.WIDTH<=32) {
      this.CIRCLEWIDTH = this.WIDTH/121*5;
    } else {
      this.CIRCLEWIDTH = this.WIDTH/121*3;
    }
    
    this.PATHWIDTH = this.CIRCLEWIDTH/1.7;
    this.coords = this.generateCoords();
  }

  generateCoords(){
    var coords = [];
    for(let col=0;col<COLS;col++){
      for (let row=0;row<ROWS;row++) {
        coords.push({
          x: row * (this.WIDTH/2) + this.CIRCLEWIDTH/2,
          y: col * (this.HEIGHT/2) + this.CIRCLEWIDTH/2,
          dot: null
        });
      }
    }
    return coords;
  }

  drawGrid() {
    this.coords.forEach((coord, index) => {
      var newDot = this.coords[index].dot = this.drawDot(`dot-${index}`);
      newDot.move(coord.x-(this.CIRCLEWIDTH/2), coord.y-(this.CIRCLEWIDTH/2));
      this.dotsContainer.add(newDot);
    });
  }

  /**
   * addtext to the image
   * @param {string} text - svg text content
   * @param {boolean} visible - text is visible?
   */
  addText(text, visible=true) {
    const ratio = this.WIDTH/119;
    const textLogo = this.texteContainer.svg(text);
    textLogo.translate(0, this.HEIGHT + this.GUTTER);
    // textLogo.scale(ratio,ratio, 0, 0);
    if (!visible){
      this.texteContainer.opacity(0);
    }
  }

  /**
   * remove the text
   */
  removeText() {
    this.texteContainer.clear();
  }

  /**
   * @param {number} id - the coordinate to look for
   * @return {object} - the object containing coordinates
   */
  getCoord(id){
    return this.coords[id];
  }

  drawDot(id){
    var dot = this.svgjsDraw.circle(this.CIRCLEWIDTH).fill('black').attr('id', id);
    return dot;
  }

  showText(){
    if (this.options.hasText){
      this.texteContainer.animate(500).opacity(1);
    }
  }

  /**
   * generate lines coordinates based on short syntax or return self
   * @param {object[]|number[]} dots - the input dots properties
   * @return {object[]}
   */
  generateLine(dots){
    for (let d of dots){
      if (typeof d == 'object')
        return dots;
    }
    return this.generateSingleLine(dots);
  }

  /**
   * generate lines coordinates based on short syntax
   * @param {object[]|number[]} dots - the input dots properties
   * @return {object[]}
   */
  generateSingleLine(dots){
    var paths = [];
    var {coords} = this; 
    dots.forEach((dot, i) => {
      if (i+1<dots.length){
        paths.push([dot, dots[i+1]]);
      }
    });
    return paths;
  }

  /**
   * simple print of the logo (noa animation)
   * @return {string} - svg string
   */
  print() {
    const {m, v} = this;
    this.draw(m);
    this.draw(v);
    if (this.options.hasText){
      this.texteContainer.opacity(1);
    }
    return this.svgjsDraw;
  }


  /**
   * switch color scheme
   */
  switchColor(scheme) {
    const {m, v} = this;
    this.options.colorScheme = scheme;
    switch (scheme) {
      case 'white':
        m.color = MCOLORW;
        v.color = VCOLORW;
        break;
      case 'black':
        m.color = MCOLORB;
        v.color = VCOLORB;
        break;
      default:
        m.color = MCOLOR;
        v.color = VCOLOR;
        break;
    }
  }

  startLoop(){
    const {m,v} = this;
    this.draw(m, () => 
      this.draw(v, () => {
        this.showText();
        setTimeout(() => {
          this.undraw(v);
          this.undraw(m, () => {
            setTimeout(() => {
              const newM = getRandomM(); 
              const newV = getRandomV(); 
              this.generateNewLetters(newM, newV);
              this.startLoop();
            }, this.options.transitionBetween)
          });
        }, this.options.transition);
      })
    );
  }

  /**
   * draw a letter
   * @param {<Letter>} letter - the letter instance to print
   * @param {function} onComplete - is triggered after draw
   */
  draw(letter, onComplete=null){
    letter.draw(onComplete);
  }

  /**
   * undraw a letter
   * @param {<Letter>} letter - the letter instance to print
   * @param {function} onComplete - is triggered after undraw
   */
  undraw(letter, onComplete=null){
    letter.undraw(onComplete);
  }

  /** 
   * remove letters
   */
  clear() {
    this.m.remove();
    this.v.remove();
    this.mContainer.clear();
    this.vContainer.clear();
    this.dotsContainer.clear();
  }

  /**
   * @param {number[]} m - m coordinates
   * @param {number[]} v - v coordinates
   */
  generateNewLetters(m=null, v=null){
    var newM, newV;
    if (m==v){
      this.step = this.step+1;
      newM = M[this.step];
      newV = V[this.step];
    } else {
      newM = m;
      newV = v;
    }

    const mCoords = this.generateLine(newM);
    const vCoords = this.generateLine(newV);

    this.m.setPathWidth(this.PATHWIDTH);
    this.v.setPathWidth(this.PATHWIDTH);
    this.m.createLetter(mCoords);
    this.v.createLetter(vCoords);
  }
}
module.exports.Logo = Logo;