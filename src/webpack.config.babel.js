import {loader} from 'webpack-partial';

const svgInline = loader({
  test: /\.svg$/,
  use: 'svg-inline-loader'
});

export default svgInline;