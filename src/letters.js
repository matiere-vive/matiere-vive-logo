// 0     1     2

// 3     4     5

// 6     7     8

const ms = [
  [6,3,7,5,8], // 0
  [6,3,7,1,8], // 1
  [6,3,4,1,2], // 2
  [6,0,7,2,8], // 3
  [6,0,4,2,8], // 4
  [6,0,4,2,5], // 5
  [6,2,7,5,8], // 6
  [3,0,4,2,5], // 7
  [3,0,4,2,8], // 8
  [3,0,7,4,8], // 9
  [3,0,4,1,5], // 10
  [3,0,4,1,8],
  [7,3,4,1,2],
  [7,3,4,1,5],
  [7,3,4,1,8],
  [6,0,4,1,2],
  [6,3,7,1,5],
  [[6,0],
   [0,2],
   [[1,7],[2,8]]],
  [[6,0],
   [0,2],
   [[1,7],[2,5]]],
  [[6,0],
   [0,2],
   [[1,4],[2,7]]],
  [[6,0],
   [0,2],
   [[1,4],[2,8]]],
  [[3,0],
   [0,2],
   [[1,4],[2,5]]],
  [[3,0],
   [0,2],
   [[1,7],[2,5]]],
];

const vs = [
  [0,3,1], // 0
  [0,3,2], // 1
  [0,6,1], // 2
  [0,6,2], // 3
  [0,7,1], // 4
  [0,7,2], // 5
  [0,7,5], // 6
  [3,6,2], // 7
  [3,7,5], // 8
  [3,7,2], 
  [3,7,4],
  [3,8,1],
  [3,8,2],
  [4,7,5],
  [4,7,2],
  [4,8,5],
  [4,8,2],
  [1,7,2],
  [1,7,5],
  [1,6,5],
  [1,4,2],
  [1,8,5],
  [1,8,2],
  [0,8,2],
];

const V = [
  vs[0],
  vs[5],
  // can't be done
  vs[7],
  vs[8],
  vs[5],
  // can't be done
  // can't be done
  vs[9],
  vs[0],
  vs[4],
];
module.exports.V = V;

const M = [
  ms[6],
  ms[0],
  // can't be done
  ms[7],
  ms[16],
  ms[3],
  // can't be done
  // can't be done
  ms[9],
  ms[10],
  ms[4],
];
module.exports.M = M;

function pickRandom(items){
  return items[Math.floor(Math.random()*items.length)];
}

const getRandomM = function(){
  return pickRandom(ms);
}
module.exports.getRandomM = getRandomM;

const getRandomV = function(){
  return pickRandom(vs);
}
module.exports.getRandomV = getRandomV;